import "./App.css"
import React from "react"
import { Login } from "./components/login";
import { BrowserRouter, Route, Routes } from "react-router-dom";

function App() {
  
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Login/>}></Route>
      </Routes>
    </BrowserRouter>
  )
}


/* <Route path="/" element={
          <ProtectedRoute>
          </ProtectedRoute>
        }></Route> */

 /* const ProtectedRoute = ({ redirectPath = "/", children }) => {

  const navigate = useNavigate()
  const user = JSON.parse(localStorage.getItem("user"))

  useEffect(() => {
    const token = localStorage.getItem("firebaseToken")
    if (token) {
      auth.onAuthStateChanged((user) => {
        if (!user) {
          navigate(redirectPath)
        }
      })
      if (user.role != "USER_ROLE") {
        navigate("/error")
      }
    } else {
      navigate(redirectPath)
    }
  }, [])

  return children
}  */

export default App
