// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
import { getAuth, GoogleAuthProvider } from 'firebase/auth';
import { getFirestore } from 'firebase/firestore';

const firebaseConfig = {
  apiKey: "AIzaSyA4uh6Z0fsVGyjetiZQQvYFAF4R3Z8M3vw",
  authDomain: "login-uade.firebaseapp.com",
  projectId: "login-uade",
  storageBucket: "login-uade.appspot.com",
  messagingSenderId: "335399100291",
  appId: "1:335399100291:web:0ba5a1a39af89f169120b6",
  measurementId: "G-BD8FMT0JT0"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);
export const googleProvider = new GoogleAuthProvider();
export const db = getFirestore(app);