import { auth, googleProvider, db } from "../configuracion/firebase";
import {  doc, setDoc } from "firebase/firestore";
/* import { useEffect } from "react"; */
import axios from 'axios';
/* import { useNavigate } from "react-router-dom"; */
import { signInWithPopup, signOut } from "firebase/auth";
import { Button } from "primereact/button";

export const Login = () => {
  /*  const navigate = useNavigate();

  useEffect(() => {
    const token = localStorage.getItem("firebaseToken");
    if (token) {
      navigate("/");
    } 
  }, []); */

  const handleSignIn = async () => {
    try {
      const result = await signInWithPopup(auth, googleProvider);
      const idToken = await result.user.getIdToken();
      
      localStorage.setItem("firebaseToken", idToken);
  
      const user = result.user;
  
      await setDoc(doc(db, "Usuarios", user.uid), {
        nombre: user.displayName,
        correo: user.email,
        foto: user.photoURL
      });
  
      let response = await axios.post("http://localhost:3001/api/login", { firebaseToken: idToken });
      localStorage.setItem("token", response.data.token);
      localStorage.setItem("user", JSON.stringify(response.data.usuario));
    } catch (error) {
      console.error('Error en el inicio de sesión con Google: ', error);
    }
  }

  const handleLogout = async () => {
    try {
      await signOut(auth);
      localStorage.removeItem("firebaseToken");
    } catch (error) {
      console.error('Error al cerrar sesión: ', error);
    }
  }

  return (
    <div className='flex justify-content-center align-items-center m-4'>
      <Button onClick={handleSignIn} label="Sign in with Google" className='m-2' severity="info" icon='pi pi-google' rounded />
      <Button onClick={handleLogout} label="Sign out" severity="danger" icon='pi pi-trash' rounded />
    </div>
  );
}
