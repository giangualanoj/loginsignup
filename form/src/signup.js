import React, { useState } from 'react';
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
import axios from 'axios';
import { Link } from 'react-router-dom';



export default function Signup() {
    const [nombre, setNombre] = useState('')
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')


    const handleSignup = async (e) => {
        e.preventDefault()
        const response = await axios.post('http://localhost:3000/usuarios', { nombre, email, password })
        if (response) {
            localStorage.setItem('token_test', response.data.token)
        }
    }

    return (
        <form onSubmit={handleSignup}>
            <div className="card flex align-items-center justify-content-center">
                <div className="surface-card p-4 shadow-2 border-round w-full lg:w-6">
                    <div className="text-center mb-5">
                        <div className="text-900 text-3xl font-medium mb-3">Signup</div>
                        <Link to='./login'>
                            <Button label="Login" text />
                        </Link>
                    </div>

                    <div>
                        <label htmlFor="nombre" className="block text-900 font-medium mb-2">Nombre</label>
                        <InputText id="nombre" value={nombre} type="text" placeholder="Nombre" onChange={e => setNombre(e.target.value)} className="w-full mb-3" />

                        <label htmlFor="email" className="block text-900 font-medium mb-2">Email</label>
                        <InputText id="email" value={email} type="text" placeholder="Email address" onChange={e => setEmail(e.target.value)} className="w-full mb-3" />

                        <label htmlFor="password" className="block text-900 font-medium mb-2">Password</label>
                        <InputText id="password" value={password} type="password" placeholder="Password" onChange={e => setPassword(e.target.value)} className="w-full mb-3" />

                        <Button label="Signup" icon="pi pi-check" type='submit' className="w-full" />

                    </div>
                </div>
            </div>
        </form>

    )
}


