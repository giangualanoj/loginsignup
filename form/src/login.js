import React, { useState } from 'react';
import { InputText } from 'primereact/inputtext';
import { Button } from 'primereact/button';
import axios from 'axios';
import { useNavigate, Link } from 'react-router-dom';



export default function Login() {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const navigate = useNavigate()

    const handleLogin = async (e) => {
        e.preventDefault()
        try {
            const response = await axios.post('http://localhost:3000/auth/login', { email, password })
            const token = response.data.token
            localStorage.setItem('token_test', token)
            navigate('/registro')
        } catch (e) {
            console.log('no se pudo iniciar sesión', e)
        }
    }

    return (
        <form onSubmit={handleLogin}>
            <div className="card flex align-items-center justify-content-center">
                <div className="surface-card p-4 shadow-2 border-round w-full lg:w-6">
                    <div className="text-center mb-5">
                        <div className="text-900 text-3xl font-medium mb-3">Login</div>
                        <Link to='./signup'>
                            <Button label="Singup" text />
                        </Link>
                    </div>

                    <div>
                        <label htmlFor="email" className="block text-900 font-medium mb-2">Email</label>
                        <InputText id="email" value={email} type="text" placeholder="Email address" onChange={e => setEmail(e.target.value)} className="w-full mb-3" />

                        <label htmlFor="password" className="block text-900 font-medium mb-2">Password</label>
                        <InputText id="password" value={password} type="password" placeholder="Password" onChange={e => setPassword(e.target.value)} className="w-full mb-3" />

                        <Button label="Login" icon="pi pi-user" type='submit' className="w-full" />
                    </div>
                </div>
            </div>
        </form>


    )
}


