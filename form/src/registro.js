import React, { useState, useEffect } from 'react';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import axios from 'axios';



export default function Registro() {
    const token = localStorage.getItem('token_test')
    const [usuarios, setUsuarios] = useState([]);
    const columns = [
        { field: 'id', header: 'ID' },
        { field: 'nombre', header: 'Nombre' },
        { field: 'email', header: 'Email' },
        { field: 'password', header: 'Password' }
    ];

    useEffect(() => {
        axios.get('http://localhost:3000/usuarios', { headers: { Authorization: `Bearer ${token}` } }).then(r => {
            setUsuarios(r.data)
        }).catch(error => console.log(error))
    }, [token])

    return (
        <div className="card">
            <DataTable value={usuarios} tableStyle={{ minWidth: '50rem' }}>
                {columns.map((col, i) => (
                    <Column key={col.field} field={col.field} header={col.header} />
                ))}
            </DataTable>
        </div>
    )
}


