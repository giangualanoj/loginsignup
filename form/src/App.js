import './App.css';
import { BrowserRouter, /* Navigate ,*/ Routes, Route } from 'react-router-dom';
import Login from './login';
import Signup from './signup';
import Registro from './registro';


function App() {
  return (
    <BrowserRouter>
    <Routes>
      <Route path='/signup' element={<Signup/>}></Route>
      <Route path='/login' element={<Login/>}></Route>
      <Route path='/' element={<Registro/>}></Route>
      {/* <Route path='/registro' element={<ProtectedRouter><Registro/></ProtectedRouter>}></Route>  */}
    </Routes>
    </BrowserRouter>
  );

}

/* const ProtectedRouter = ({redirectPath = '/login', children}) => {
  const token = localStorage.getItem('token_test')
  
  if (!token) {
    return <Navigate to={redirectPath} />
  }
  return children
} */

export default App;
