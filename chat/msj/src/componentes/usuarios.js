import React, { useState, useEffect } from 'react';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import { db } from '../configuracion/firebase';
import { collection, query, onSnapshot } from "firebase/firestore";

export default function Usuarios() {
    const [usuarios, setUsuarios] = useState([]);
/*     const [loading, setLoading] = useState(null); */

    useEffect(() => {
        const q = query(collection(db, 'Usuarios'));
        const data = onSnapshot(q, (QuerySnapshot) => {
            let usuario = [];
            QuerySnapshot.forEach((doc) => {
                usuario.push({ ...doc.data(), id: doc.id});
            });
            setUsuarios(usuario)

        });
        return () => data;

    }, []);


    const imageBodyTemplate = (usuario) => {
        return <img src={usuario.photoURL} alt={usuario.nombre} className="w-6rem shadow-2 border-round" />;
    };

    return (
        <div className="card">
            <DataTable value={usuarios} tableStyle={{ minWidth: '50rem' }} /* loading={loading} */>
                <Column field="nombre" header="Nombre"></Column>
                <Column field="mail" header="Mail"></Column>
                <Column header="Foto" body={imageBodyTemplate}></Column>
            </DataTable>
        </div>
    );
}
        