import axios from 'axios';
import { auth, googleProvider } from '../configuracion/firebase';
import { signInWithPopup, signOut } from 'firebase/auth';
import { Button } from 'primereact/button';
import { useNavigate } from 'react-router-dom';
import { db } from '../configuracion/firebase';
import React, { useEffect } from 'react';
import { doc, setDoc } from "firebase/firestore";

export function Auth() {
    const navigate = useNavigate();

    useEffect(() => {
        if (localStorage.getItem("firebaseToken")) {
            navigate("/componentes/chat")
        }
    }
    )


    const handleSignIn = async () => {
        try {
            const result = await signInWithPopup(auth, googleProvider);
            const idToken = await result.user.getIdToken();

            localStorage.setItem("firebaseToken", idToken);

            const user = result.user;

            await setDoc(doc(db, "Usuarios", user.uid), {
                imgUrl: user.photoURL,
                mail: user.email,
                nombre: user.displayName
            });

            let response = await axios.post("http://localhost:3001/api/login", { firebaseToken: idToken });
            localStorage.setItem("token", response.data.token);
            localStorage.setItem("user", JSON.stringify(response.data.usuario));

            navigate("/componentes/chat");

        } catch (error) {
            console.error('Error en el inicio de sesión con Google: ', error);
        }
    }

    const handleSignout = async () => {
        try {
            await signOut(auth);
            localStorage.removeItem("firebaseToken");
        } catch (error) {
            console.error('Error al cerrar sesión: ', error);
        }
    }

    return (
        <div className='flex justify-content-center align-items-center m-4'>
            <Button onClick={handleSignIn} label="Sing in with Google" className='m-2' severity="info" icon='pi pi-google' rounded />
            <Button onClick={handleSignout} label="Sing out" severity="danger" icon='pi pi-trash' rounded />
        </div>
    )
}


