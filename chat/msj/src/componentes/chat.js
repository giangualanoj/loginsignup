import React, { useState, useEffect } from 'react';
import { db, auth } from '../configuracion/firebase';
import { Mensajes } from '../componentes/mensajes';
import { collection, query, onSnapshot } from "firebase/firestore";
import Usuarios from './usuarios';


export const Chat = () => {
    const [mensajes, setMensajes] = useState([])
    const id = auth.currentUser ? auth.currentUser.uid : null;

    useEffect(() => {
        const q = query(collection(db, 'Mensajes'));
        const data = onSnapshot(q, (QuerySnapshot) => {
            let mensajes = [];
            QuerySnapshot.forEach((doc) => {
                mensajes.push({ ...doc.data(), id: doc.id, fecha: new Date(doc.data().fecha.toDate()) });
            });
            setMensajes(mensajes)

        });
        return () => data;

    }, []);


    const dateTemplate = (rowData) => {
        return (
            <React.Fragment>
                <div>{rowData.fecha.toDateString()}</div>
            </React.Fragment>
        )
    }


    return (
        <div>
            <div className='card'>
                <div className='flex justify-content-center' >
                    <h2>Chat</h2>
                </div>

                {mensajes && mensajes.map((mensaje) =>
                    <div
                        key={mensaje.usuario}
                        className={`msj ${id === mensaje.usuario_id ? 'sent' : 'received'}`}>
                        <p>{mensaje.mensaje}</p>
                        <p>{dateTemplate}</p>
                    </div>
                )}
                <Mensajes />
            </div>
            <Usuarios/>
        </div>
    )

}

