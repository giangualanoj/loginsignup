import React, { useState } from 'react';
import { InputText } from 'primereact/inputtext';
import { db, auth } from '../configuracion/firebase';
import { collection, addDoc } from 'firebase/firestore';
import { Button } from 'primereact/button';


export const Mensajes = () => {
    const [value, setValue] = useState('');
    const mensajeRef = collection(db, 'Mensajes');

    const sendMsj = async () => {
        try {
            const user = auth.currentUser;

            console.log(user)
            if (!user) {
                console.error('El usuario no está autenticado.');
                return;
            }

            const { uid } = user;
            if (!value) {
                console.error('El mensaje está vacío.');
                return;
            }

            const mensajes = { mensaje: value, usuario: uid, fecha: new Date()}
            console.log(mensajes)
            await addDoc(mensajeRef, mensajes);

            setValue('');
        } catch (error) {
            console.error('Error al enviar el mensaje:', error);
        }
    };

    return (
        <div className="flex justify-content-center mt-8 pt-8">
            <InputText value={value} onChange={(e) => setValue(e.target.value)} />
            <Button icon="pi pi-send" onClick={sendMsj} rounded text raised severity="success" aria-label="send" className='mx-3' />
        </div>
    );
};
