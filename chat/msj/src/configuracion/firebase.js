// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
import { getAuth, GoogleAuthProvider } from 'firebase/auth';
import { getFirestore } from 'firebase/firestore';

const firebaseConfig = {
    apiKey: "AIzaSyB4ihkjr5KQv7tZEtUZjFi-TE2RrtYJnGo",
    authDomain: "conversacion-uade-2.firebaseapp.com",
    projectId: "conversacion-uade-2",
    storageBucket: "conversacion-uade-2.appspot.com",
    messagingSenderId: "829183004099",
    appId: "1:829183004099:web:1b78162f723828af788176"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export const auth = getAuth(app);
export const googleProvider = new GoogleAuthProvider();
export const db = getFirestore(app);