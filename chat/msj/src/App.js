import './App.css';
import React, { useEffect } from 'react';
import { BrowserRouter, Routes, Route, useNavigate } from 'react-router-dom';
import { Auth } from './componentes/auth';
import { auth } from './configuracion/firebase';
import { Chat } from './componentes/chat';

function App() {

  return (
    <BrowserRouter>
      <Routes>
        <Route path='/' element={<Auth />}></Route>
        <Route path='/componentes/chat' element={<ProtectedRoute>
          <Chat />
        </ProtectedRoute>}>
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

const ProtectedRoute = ({ redirectPath = "/", children }) => {
  const navigate = useNavigate()

  useEffect(() => {
    const token = localStorage.getItem("firebaseToken")
    if (token) {
      auth.onAuthStateChanged((user) => {
        if (!user) {
          navigate(redirectPath)
        }
      })
    } else {
      navigate(redirectPath)
    }
  }, [])

  return children
}

export default App;
